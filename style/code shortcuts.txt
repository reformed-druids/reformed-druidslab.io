BULLET LIST FORMATTING
        <h2>Browser Compatibility</h2>
        <p>This template has been tested in the following browsers:</p>
        <ul>
          <li>Internet Explorer 9</li>
          <li>FireFox 25</li>
          <li>Google Chrome 31</li>
        </ul>

NUMBERED LIST FORMATTING
        <ul>
          <li>Item 1</li>
          <li>Item 2</li>
          <li>Item 3</li>
          <li>Item 4</li>
        </ul>

TWO COLUMN TABLE DESIGN
        <h2>Tables</h2>
        <table style="width:100%; border-spacing:0;">
          <tr><th>Item</th><th>Description</th></tr>
          <tr><td>Item 1</td><td>Description of Item 1</td></tr>
          <tr><td>Item 2</td><td>Description of Item 2</td></tr>
          <tr><td>Item 3</td><td>Description of Item 3</td></tr>
          <tr><td>Item 4</td><td>Description of Item 4</td></tr>
        </table>

TEXT BODY BACKGROUND COLOR (SAGE GREEN)
<div style="background-color: #ebebe0">

INLINE FONT COLOR CHANGE
<font color=FF0000>REDTEXTISRED</font>

ANCHOR 2.0
<a href="#TAG">VISIBLE TABLE OF CONTENTS TEXT TO BE LINKED TO ANCHOR</a>
<p ID="TAG"></p>

FAVICON - TINY PIC IN BROWSER TAB - INSERT UNDER <HEAD>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

LINKING TO ANOTHER PAGE
<a href="PAGE.html">DISPLAYEDLINKTEXT</a>

LINKING TO A SUBFOLDER FILE
<a href="files//FILENAMEdotEXTENSION">LINKTEXTDISPLAYED</a>

INKING TO EXTERNAL SITE
<a href="https://www.facebook.com">ieFACEBOOK</a>

CHANGE FONT
<p style = "font-family:FONTNAME;">TEXTDISPLAYEDASDESIREDFONT</p>

